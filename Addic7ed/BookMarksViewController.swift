//
//  BookMarksViewController.swift
//  
//
//  Created by Marentdev on 04/10/2017.
//

import UIKit

class BookMarksViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var fav_versions:[String] = []
    var fav_links:[String] = []
    var fav_languages:[String] = []
    var ready:Bool = false
    var fav_end_version:[String] = []
    var fav_end_language:[String] = []
    var fav_end_progress:[String] = []
    var fav_names:[String] = []
    @IBOutlet weak var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "BookMarks"
        self.setup()
    }
    func setup() {
        fav_versions = save.array(forKey: "versions") == nil ? []:save.array(forKey: "versions") as! [String]
        fav_links = save.array(forKey: "links") == nil ? []:save.array(forKey: "links") as! [String]
        fav_languages = save.array(forKey: "languages") == nil ? []:save.array(forKey: "languages") as! [String]
        fav_names = save.array(forKey: "names") == nil ? []:save.array(forKey: "names") as! [String]
        preparefav()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fav_end_version.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellmulti", for: indexPath) as! BookMarksTableViewCell
        if ready {
            cell.Version.text = Clean_string(text: fav_end_version[indexPath.row])
            cell.Language.text = Clean_string(text: fav_end_language[indexPath.row])
            cell.ProgressText.text = "\(Clean_string(text: fav_end_progress[indexPath.row]))%"
            cell.ProgressBar.progress = Float(fav_end_progress[indexPath.row])! / 100
            cell.Name.text = Clean_string(text: fav_names[indexPath.row])
        }
        return cell
    }
    func preparefav() {
        self.fav_end_version.removeAll()
        self.fav_end_language.removeAll()
        self.fav_end_progress.removeAll()
        for i in 0 ..< fav_versions.count {
            var version:[String] = []
            var language:[String] = []
            var progress:[String] = []
            session.request(URL(string: fav_links[i])!).responseString { (result) in
                let html:String = result.result.value!.remove(text: ["\"", "\n", "\t", "\r"])
                let block = matches(for: "<div id=container95m><table class=tabel95>(.*?)</ta", in: html)
                
                for k in 0 ..< block.count {
                    let lang = matches(for: "class=language>(.*?)<a", in: block[k])
                    for l in 0 ..< lang.count {
                        if !lang[l].isEmpty {
                            language.append(lang[l].remove(text: ["class=language>", "<a"]))
                        }
                    }
                    let name = matches(for: "width=16 height=16 />(.*?)<", in: block[k])
                    for l in 0 ..< name.count {
                        if !name[l].isEmpty {
                            version.append(name[l].remove(text: ["width=16 height=16 />","<"]))
                        }
                    }
                    if lang.count > 1 {
                        for _ in 1 ..< lang.count {
                            if !lang[0].isEmpty {
                                version.append(name[0].remove(text: ["width=16 height=16 />","<"]))
                            }
                        }
                    }
                    let prog = matches(for: "<td width=19%><b>(.*?)</b>", in: block[k])
                    for l in 0 ..< prog.count {
                        if !prog[l].isEmpty {
                            progress.append(prog[l].remove(text: ["<td width=19%><b>", "</b>", " ", "%"]) == "Completed" ? "100":prog[l].remove(text: ["<td width=19%><b>", "</b>", " ", "%", "Completed"]))
                        }
                    }
                }
                for j in 0 ..< version.count {
                    if version[j] == self.fav_versions[i] && language[j] == self.fav_languages[i] {
                        self.fav_end_version.append(version[j])
                        self.fav_end_language.append(language[j])
                        self.fav_end_progress.append(progress[j])
                    }
                }
                if i + 1 == self.fav_versions.count {
                    self.ready = true
                    self.table.reloadData()
                }
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, index) in
            self.delete(id: index.row)
        }
        return [delete]
    }
    func delete(id:Int) {
        fav_names.remove(at: id)
        fav_end_language.remove(at: id)
        fav_end_version.remove(at: id)
        fav_links.remove(at: id)
        save.set(fav_end_version, forKey: "versions")
        save.set(fav_links, forKey: "links")
        save.set(fav_end_language, forKey: "languages")
        save.set(fav_names, forKey: "names")
        save.synchronize()
        self.setup()
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}
