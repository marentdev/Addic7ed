//
//  Functions.swift
//  Addic7ed
//
//  Created by Marentdev on 29/09/2017.
//  Copyright © 2017 Marentdev. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
let session:SessionManager = SessionManager.default
let save = UserDefaults.standard
extension String {
    func htmlAttributedString() -> NSAttributedString? {
        guard let data = self.data(using: String.Encoding.utf16, allowLossyConversion: false) else { return nil }
        guard let html = try? NSMutableAttributedString(
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil) else { return nil }
        return html
    }
    func remove(text:[String]) -> String{
        var t:String = self
        for i in 0 ..< text.count {
            t = t.replacingOccurrences(of: text[i], with: "")
        }
        return t
    }
}
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
func Clean_string(text:String) -> String{
    let decodedString = text.htmlAttributedString()?.string
    return decodedString! as String
}
func matches(for regex: String!, in text: String!) -> [String] {
    
    do {
        let regex = try NSRegularExpression(pattern: regex, options: [])
        let nsString = text as NSString
        let results = regex.matches(in: text, range: NSMakeRange(0, nsString.length))
        return results.map { nsString.substring(with: $0.range)}
    } catch let error as NSError {
        print("invalid regex: \(error.localizedDescription)")
        return []
    }
}
func GetElement(input:NSString, paternn:NSString) -> NSString {
    let azerty:NSString = input as NSString
    let cowRegex = try! NSRegularExpression(pattern: paternn as String,
                                            options: .caseInsensitive)
    if let cowMatch = cowRegex.firstMatch(in: azerty as String, options: [],
                                          range: NSMakeRange(0, azerty.length)) {
        return (azerty as NSString).substring(with: cowMatch.range) as NSString
    }else{
        return ""
    }
}
