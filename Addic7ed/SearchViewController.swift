//
//  SearchViewController.swift
//  Addic7ed
//
//  Created by Marentdev on 01/10/2017.
//  Copyright © 2017 Marentdev. All rights reserved.
//

import UIKit
class SearchViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var SearchBar: UISearchBar!
    var ready:Bool = false
    var names:[String] = []
    var link:[String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Search"
        self.SearchBar.barTintColor = self.navigationController?.navigationBar.barTintColor
    }
    func preparelist(keyword:String) {
        self.names.removeAll()
        self.link.removeAll()
        session.request(URL(string: "http://addic7ed.com/search.php?search=\(keyword.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlPathAllowed)! )&Submit=Search")!).responseString { (result) in
            let html:String = result.result.value!.remove(text: ["\"", "\n", "\t", "\r"])
            if !html.contains("Sorry, your search") && !html.contains("returned zero results") {
                let travaille_name = matches(for: "debug=(.*?)</a", in: html)
                for i in 0 ..< travaille_name.count {
                    self.names.append((GetElement(input: travaille_name[i] as NSString, paternn: ">(.*?)<") as String).remove(text: [">", "<"]))
                }
                let links = matches(for: "</td><td><a href=(.*?) debug=", in: html)
                for i in 0 ..< links.count {
                    self.link.append(links[i].remove(text: ["</td><td><a href=", " debug="]))
                }
                self.ready = true
                self.table.reloadData()
            }else {
                self.ready = false
                self.table.reloadData()
                let alert = UIAlertController(title: "No result", message: "No result found for: \(keyword)", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            self.removeAllOverlays()
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //Do action
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.showWaitOverlayWithText("Loading...")
        self.SearchBar.resignFirstResponder()
        preparelist(keyword: searchBar.text!)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        if ready {
            cell.textLabel?.text = names[indexPath.row]
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(link[indexPath.row])
        link_single = "http://addic7ed.com/\(link[indexPath.row])"
        Season_name = names[indexPath.row]
        self.performSegue(withIdentifier: "see", sender: self)
        self.table.deselectRow(at: indexPath, animated: true)
    }
}

