//
//  ViewController.swift
//  Addic7ed
//
//  Created by Marentdev on 26/09/2017.
//  Copyright © 2017 Marentdev. All rights reserved.
//

import UIKit
import Alamofire
var link_multi:String = "http://www.addic7ed.com/ajax_loadShow.php?show=marent&season=nums&langs=|&hd=0&hi=0"
var link_single:String = "http://www.addic7ed.com/re_episode.php?ep=acounamatata"
var Season_name:String = ""
class ViewController: UIViewController, SWComboxViewDelegate {

    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var Clear: UIButton!
    @IBOutlet weak var Legende: UILabel!
    @IBOutlet weak var containner1: UIView!
    var SeriesCombo:SWComboxView = SWComboxView()
    var Series_name:[String] = []
    var Series_id:[String] = []
    var Series_seasons_num:[String] = []
    var Series_Episode_name:[String] = []
    var Series_Episode_id:[String] = []
    var Link:String = "http://www.addic7ed.com/ajax_getShows.php"
    var cometoback:Bool = false
    @IBOutlet weak var followedlabel: UILabel!
    /* Fav */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCombox()
        /* DELETE */
        /*save.set([] as [String], forKey: "versions")
        save.set([] as [String], forKey: "links")
        save.set([] as [String], forKey: "languages")
        save.set([] as [String], forKey: "names")
        save.set([] as [String], forKey: "authors")*/
        save.synchronize()
        /* END DELETE */
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if cometoback {
            self.showWaitOverlayWithText("Loading...")
            Link = "http://www.addic7ed.com/ajax_getShows.php"
            setupCombox()
            cometoback = false
        }
        isfast = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func CLearCombo(_ sender: UIButton) {
        if Link.contains("ajax_getSeasons") {
            Link = "http://www.addic7ed.com/ajax_getShows.php"
            setupCombox()
            self.Legende.text = "Choose a series:"
        } else if Link.contains("ajax_getEpisodes") {
            self.Legende.text = "Choose a season:"
            Link = Link.replacingOccurrences(of: "ajax_getEpisodes", with: "ajax_getSeasons")
            let a:String = GetElement(input: Link as NSString, paternn: "&s(.*)") as String
            Link = Link.remove(text: [a])
            setupComboxSeasons()
        }
    }
    
    func setupCombox()
    {
        var helper: SWComboxTitleHelper
        helper = SWComboxTitleHelper()
        SeriesCombo = SWComboxView.loadInstanceFromNibNamedToContainner(container: self.containner1)! as! SWComboxView
        session.request(URL(string: Link)!).responseString { (result) in
            let html:String = result.result.value!.replacingOccurrences(of: "\"", with: "")
            let id:[String] = matches(for: "<option value=(.*?)>", in: html)
            self.Series_id.removeAll()
            for i in 0 ..< id.count {
                self.Series_id.append(id[i].remove(text: ["<option value=", ">", " "]))
            }
            let name:[String] = matches(for: " >(.*?)</option>", in: html)
            self.Series_name.removeAll()
            for i in 0 ..< name.count {
                self.Series_name.append(name[i].remove(text: ["</option>", " >"]))
            }
            self.SeriesCombo.bindData(data: self.Series_name as NSArray, comboxHelper: helper, seletedIndex: 1, comboxDelegate: self, containnerView: self.view)
            self.removeAllOverlays()
        }
        SeriesCombo.delegate = self
    }
    func setupComboxSeasons() {
        var helper: SWComboxTitleHelper
        helper = SWComboxTitleHelper()
        SeriesCombo = SWComboxView.loadInstanceFromNibNamedToContainner(container: self.containner1)! as! SWComboxView
        session.request(URL(string: Link)!).responseString { (result) in
            let html:String = result.result.value!.replacingOccurrences(of: "\"", with: "")
            let name:[String] = matches(for: "<option(.*?)</option>", in: html)
            self.Series_seasons_num.removeAll()
            for i in 0 ..< name.count {
                self.Series_seasons_num.append(name[i].remove(text: ["</option>", "<option>","<option selected>"]))
            }
            if self.Series_seasons_num.contains("<option value=0>Season") {
                self.Series_seasons_num.remove(at: 0)
            }
            self.SeriesCombo.bindData(data: self.Series_seasons_num as NSArray, comboxHelper: helper, seletedIndex: 1, comboxDelegate: self, containnerView: self.view)
        }
        SeriesCombo.delegate = self
    }
    func setupComboxEpisodes() {
        var helper: SWComboxTitleHelper
        helper = SWComboxTitleHelper()
        SeriesCombo = SWComboxView.loadInstanceFromNibNamedToContainner(container: self.containner1)! as! SWComboxView
        session.request(URL(string: Link)!).responseString { (result) in
            let html:String = result.result.value!.replacingOccurrences(of: "\"", with: "")
            let id:[String] = matches(for: "<option value=(.*?)>", in: html)
            self.Series_Episode_id.removeAll()
            for i in 0 ..< id.count {
                self.Series_Episode_id.append(id[i].remove(text: ["<option value=", ">", " "]))
            }
            self.Series_Episode_id.remove(at: 0)
            let name:[String] = matches(for: ">(.*?)</option>", in: html)
            self.Series_Episode_name.removeAll()
            for i in 0 ..< name.count {
                let a = GetElement(input: name[i] as NSString , paternn: "<option(.*?)>")
                if !(a as String).isEmpty {
                    self.Series_Episode_name.append(name[i].remove(text: ["</option>", a as String]))
                }else {
                    self.Series_Episode_name.append(name[i].remove(text: ["</option>", ">"]))
                }
                
            }
            if self.Series_Episode_name.contains("> [Select an episode]") {
                self.Series_Episode_name.remove(at: 0)
            }
            self.SeriesCombo.bindData(data: self.Series_Episode_name as NSArray, comboxHelper: helper, seletedIndex: 1, comboxDelegate: self, containnerView: self.view)
        }
        SeriesCombo.delegate = self
        
    }
    func selectedAtIndex(index: Int, combox: SWComboxView) {
        if Link.contains("ajax_getShows") {
            combox.name.text = combox.list[index] as! String
            self.Legende.text = "Choose a season:"
            Link = Link.replacingOccurrences(of: "ajax_getShows", with: "ajax_getSeasons")
            Link += "?showID=\(self.Series_id[index + 1])"
            setupComboxSeasons()
        } else if Link.contains("ajax_getSeasons") {
            self.Legende.text = "Choose an episode:"
            combox.name.text = combox.list[index] as! String
            Link = Link.replacingOccurrences(of: "ajax_getSeasons", with: "ajax_getEpisodes")
            Link += "&season=\(self.Series_seasons_num[index])"
            setupComboxEpisodes()
        } else if Link.contains("ajax_getEpisodes") {
            combox.name.text = combox.list[index] as! String
            Season_name = combox.list[index] as! String
            if !link_multi.contains("marent") {
                link_multi = "http://www.addic7ed.com/ajax_loadShow.php?show=marent&season=nums&langs=|&hd=0&hi=0"
            }
            link_multi = link_multi.replacingOccurrences(of: "marent", with: (GetElement(input: Link as NSString, paternn: "showID=(.*?)&") as String).remove(text: ["showID=", "&"])).replacingOccurrences(of: "nums", with: (GetElement(input: Link as NSString, paternn: "&season=(.*)") as String).remove(text: ["&season="]))
            Link = "http://www.addic7ed.com/ajax_getShows.php"
            if !link_single.contains("acounamatata") {
                link_single = "http://www.addic7ed.com/re_episode.php?ep=acounamatata"
            }
            link_single = link_single.replacingOccurrences(of: "acounamatata", with: self.Series_Episode_id[index])
            self.Legende.text = "Choose a series:"
            setupCombox()
            cometoback = true
            isfast = true
            self.performSegue(withIdentifier: "multi_download", sender: self)
        }
    }
    func tapComboxToOpenTable(combox: SWComboxView) {
        //
    }
}

