//
//  SWComboxTitleHelper.swift
//  AuntFood
//
//  Created by shouwei on 3/8/15.
//  Copyright (c) 2015 shou. All rights reserved.
//

import UIKit

protocol SWComboxCommonHelper {
    
    func loadCurrentView(contentView:UIView, data: String)
    
    func setCurrentView(data: String)
    
    func getCurrentCell(tableView: UITableView, data: String) -> UITableViewCell
    
    func getCurrentTitle() -> String
    
}



class SWComboxTitleHelper: SWComboxCommonHelper {
    
    var comboxView:SWComboxTitle!
    
    func loadCurrentView(contentView:UIView, data: String)
    {
        comboxView = UIView.loadInstanceFromNibNamedToContainner(container: contentView)
        comboxView.bindTitle(title: data)
    }
    
    func setCurrentView(data: String){
        comboxView.bindTitle(title: data)
    }
    
    func getCurrentCell(tableView: UITableView, data: String) -> UITableViewCell {
        var cellFrame = comboxView.frame
        cellFrame.size.width = tableView.frame.size.width
        
        var cell = UITableViewCell()
        cell.frame = cellFrame
        cell.textLabel?.text = data
        return cell
    }
    
    func getCurrentTitle() -> String {
        return self.comboxView.name.text!
    }
    
}






