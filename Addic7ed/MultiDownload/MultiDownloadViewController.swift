//
//  MultiDownloadViewController.swift
//  Addic7ed
//
//  Created by Marentdev on 30/09/2017.
//  Copyright © 2017 Marentdev. All rights reserved.
//

import UIKit
var isfast:Bool = false
class MultiDownloadViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var NavBar: UINavigationItem!
    @IBOutlet weak var table: UITableView!
    var ready:Bool = false
    var version:[String] = []
    var language:[String] = []
    var progress:[String] = []
    var author:[String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        NavBar.title = Season_name
        prepare_page()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func prepare_page() {
        self.showWaitOverlayWithText("Loading...")
        session.request(URL(string: link_single)!).responseString { (result) in
            let html:String = result.result.value!.remove(text: ["\"", "\n", "\t", "\r"])
            let block = matches(for: "<div id=container95m><table class=tabel95>(.*?)</ta", in: html)
            print(html)
            for i in 0 ..< block.count {
                let lang = matches(for: "class=language>(.*?)<a", in: block[i])
                for j in 0 ..< lang.count {
                    if !lang[j].isEmpty {
                        self.language.append(lang[j].remove(text: ["class=language>", "<a"]))
                    }
                }
                let name = matches(for: "width=16 height=16 />(.*?)<", in: block[i])
                for j in 0 ..< name.count {
                    if !name[j].isEmpty {
                        self.version.append(name[j].remove(text: ["width=16 height=16 />","<"]))
                    }
                }
                let authors = matches(for: "uploaded by <a href=\'/user/(.*?)</a", in: block[i])
                for j in 0 ..< authors.count {
                    if !authors[j].isEmpty {
                        self.author.append((GetElement(input: authors[j] as NSString, paternn: ">(.*?)<") as String).remove(text: [" <", ">", "<"]))
                    }
                }
                if lang.count > 1 {
                    for _ in 1 ..< lang.count {
                            if !lang[0].isEmpty {
                            self.version.append(name[0].remove(text: ["width=16 height=16 />","<"]))
                            self.author.append((GetElement(input: authors[0] as NSString, paternn: ">(.*?)<") as String).remove(text: [" <", ">", "<"]))
                        }
                    }
                }

                let prog = matches(for: "<td width=19%><b>(.*?)</b>", in: block[i])
                for j in 0 ..< prog.count {
                    if !prog[j].isEmpty {
                        self.progress.append(prog[j].remove(text: ["<td width=19%><b>", "</b>", " ", "%"]) == "Completed" ? "100":prog[j].remove(text: ["<td width=19%><b>", "</b>", " ", "%", "Completed"]))
                    }
                }
            }
            self.ready = true
            self.table.reloadData()
            self.removeAllOverlays()
        }
    }
    @objc func tapthefollowbutton(_ sender:UIButton) {
        var versions_episode = save.array(forKey: "versions") == nil ? []:save.array(forKey: "versions")
        var links_episode = save.array(forKey: "links") == nil ? []:save.array(forKey: "links")
        var languages_episode = save.array(forKey: "languages") == nil ? []:save.array(forKey: "languages")
        var names_episode = save.array(forKey: "names") == nil ? []:save.array(forKey: "names")
        var authors_episode = save.array(forKey: "authors") == nil ? []:save.array(forKey: "authors")

        if (versions_episode as! [String]).contains(self.version[sender.tag])
            && (links_episode as! [String]).contains(link_single)
            && (languages_episode as! [String]).contains(self.language[sender.tag])
            && (names_episode as! [String]).contains(Season_name)
            && (authors_episode as! [String]).contains(self.author[sender.tag]){
            let alert = UIAlertController(title: "Follow translation", message: "This translation is allready in you BookMarks", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            versions_episode?.append(self.version[sender.tag])
            links_episode?.append(link_single)
            languages_episode?.append(self.language[sender.tag])
            names_episode?.append(Season_name)
            authors_episode?.append(self.author[sender.tag])
            save.set(versions_episode, forKey: "versions")
            save.set(links_episode, forKey: "links")
            save.set(languages_episode, forKey: "languages")
            save.set(names_episode, forKey: "names")
            save.set(authors_episode, forKey: "authors")
            save.synchronize()
            let alert = UIAlertController(title: "Follow translation", message: "This translation is now in you BookMarks", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !isfast {
            return 144
        }
        return 130
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return version.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellmulti", for: indexPath) as! MultiDownloadTableViewCell
        if ready {
            cell.Version.text = Clean_string(text: version[indexPath.row])
            cell.Language.text = Clean_string(text: language[indexPath.row])
            cell.ProgressText.text = "\(Clean_string(text: progress[indexPath.row]))%"
            cell.ProgressBar.progress = Float(progress[indexPath.row])! / 100
            cell.Author.text = self.author[indexPath.row]
            if !isfast {
                cell.Follow.addTarget(self, action: #selector(MultiDownloadViewController.tapthefollowbutton(_:)), for: UIControlEvents.touchUpInside)
                cell.Follow.tag = indexPath.row
                cell.Follow.isHidden = false
            }else {
                cell.Follow.isHidden = true
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
