//
//  MultiDownloadTableViewCell.swift
//  Addic7ed
//
//  Created by Marentdev on 30/09/2017.
//  Copyright © 2017 Marentdev. All rights reserved.
//

import UIKit

class MultiDownloadTableViewCell: UITableViewCell {

    @IBOutlet weak var Follow: UIButton!
    @IBOutlet weak var ProgressBar: UIProgressView!
    @IBOutlet weak var ProgressText: UILabel!
    @IBOutlet weak var Language: UILabel!
    @IBOutlet weak var Version: UILabel!
    @IBOutlet weak var Author: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
